# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/backblaze/b2" {
  version = "0.8.1"
  hashes = [
    "h1:8RfNGddgJ5Xu9LcA9UtzXtVV87a1UxUEWTy9+83Czoo=",
    "zh:2f4ffb6cbc1bdae18a71a9b9b2b3563c2109a31b409077bf32e0bdc5c1980605",
    "zh:87395bf0c2951770b9a764b55ccf2ac630d064219c09d6b4fad8ab0c81d6bbe9",
    "zh:c2cf5f8f349f6484ab19d3b43d9952d299ef6128a9c0b0c81c3d87c0a9bbbd97",
    "zh:f080728e857503e2dbda3f56f2b3154aae99ef0f0ef9d81163018e59c1313e1b",
  ]
}

provider "registry.terraform.io/maxlaverse/bitwarden" {
  version     = "0.6.1"
  constraints = ">= 0.1.1, >= 0.3.3"
  hashes = [
    "h1:4WyH6ZrvDozjRkLIht32XHDeikv6is4d1Ehs9vOdPak=",
    "zh:087cad2e63082253384682fb7cc104c9a9e35dbfdb22e0c79173090886988698",
    "zh:10479d1a8c961ff3a3ac51077564f1382acedcc970d220e21bc87ef69872aee9",
    "zh:3faa04a8ec43b1d4c127048e7d0a3946901cf7432275ffd19a0a0505099f620c",
    "zh:4987ffa5164342aac27868494c01ebcdcd8652de8b28edfdee77cf0f07a2124b",
    "zh:50a929299c1a1f0091e7e4c0b16ccb8d244c6346378b7808106bac3ec0dc86c3",
    "zh:52471f91b5205c67a7290b2111fe0fe6cc2f3d6002632d3c4cb7a8141ea8b48f",
    "zh:5530b2eca59a4ea35e54fd293dd4b62c5fef616f75465303b4890a530b5b07f4",
    "zh:5bd00c9b5c4ed43ddc4ad659234dc3aa39763252c89096ca9c436240d254da73",
    "zh:5bf9bb4b1aa07fbca0683abe16027e934f1a42cbc50f1c2cbcff9cd2e37d07e5",
    "zh:6af23e12679696a09e66c741fd0a49b57ed99e6e16d3735b819b34b71d871150",
    "zh:6c658522d5f5326d5bbd34fb5a6d2b523996e9fedb9d23d18aa96eb927f481f0",
    "zh:9e1bcd019aa3f01d2000bd921ff424dec9a2fa461f6d3ed3350704996c9877fc",
    "zh:c974d3ebe2d7bf663fa6e8e068e575a85091629301f9bc87c017775eb5bfaddc",
    "zh:f11ff77de854d33de6809ebecdf5cb783cfbb4bf852538d1c7cc49e7bbfb4192",
  ]
}
