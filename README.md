
## b2_application_key data structure response
```
appkeys = {
  "sample" = {
    "application_key_id" = "002b8ca855351a00000000007"
    "bucket_id" = ""
    "capabilities" = toset([
      "bypassGovernance",
      "deleteBuckets",
      "deleteFiles",
      "deleteKeys",
      "listBuckets",
      "listFiles",
      "listKeys",
      "readBucketEncryption",
      "readBucketReplications",
      "readBucketRetentions",
      "readBuckets",
      "readFileLegalHolds",
      "readFileRetentions",
      "readFiles",
      "shareFiles",
      "writeBucketEncryption",
      "writeBucketReplications",
      "writeBucketRetentions",
      "writeBuckets",
      "writeFileLegalHolds",
      "writeFileRetentions",
      "writeFiles",
      "writeKeys",
    ])
    "id" = "002b8ca855351a00000000007"
    "key_name" = "sample"
    "name_prefix" = ""
    "options" = toset([
      "s3",
    ])
  }
}
```

## b2_bucket data structure response
```
  + b2_bucket = {
      + account_id                     = "b8ca855351a0"
      + bucket_id                      = "1b981cda7875d5b365d10a10"
      + bucket_info                    = {}
      + bucket_name                    = "666-restic-backups-josh"
      + bucket_type                    = "allPrivate"
      + cors_rules                     = []
      + default_server_side_encryption = [
          + {
              + algorithm = ""
              + mode      = "none"
            },
        ]
      + file_lock_configuration        = [
          + {
              + default_retention    = []
              + is_file_lock_enabled = false
            },
        ]
      + id                             = "1b981cda7875d5b365d10a10"
      + lifecycle_rules                = []
      + options                        = []
      + revision                       = 2
    }
```

## Import terraform resources

### buckets

#### requirements
 - SET environment var for B2_APPLICATION_KEY (master key)
 - SET environment var for B2_APPLICATION_KEY_ID (master_key_id)
 - b2 cli

#### prep
Each bucket you intend to import needs to have a prexisting entry in `local.buckets` within `main.tf`.  Only the BucketID an BucketName are required.

#### script
Here's the dirty stuff.

```
for bucket in `b2 list-buckets --json | jq -cr '.[] | {bucketId, bucketName}'`
do
  bucket_id=$(echo $bucket | cut -d, -f1 | cut -d\: -f2 | cut -d\" -f2)
  bucket_name=$(echo $bucket | cut -d, -f2 | cut -d\: -f2 | cut -d\" -f2)
  terraform_import="terraform import 'module.b2_buckets[\"$bucket_name\"].b2_bucket.bucket' $bucket_id"
  
  echo $terraform_import
  eval "$terraform_import"
done
 ```