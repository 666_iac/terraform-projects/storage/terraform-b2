locals {
  app_keys = {
    "backups-duplicacy": {
      "key_name": "backups-duplicacy",
      "capabilities": [
        "listBuckets",
        "readBuckets",
        "listFiles",
        "readFiles",
        "shareFiles",
        "writeFiles",
        "deleteFiles",
        "readBucketEncryption",
        "writeBucketEncryption"
      ],
      "bucket_id": "666-backups-duplicacy-main",
      "name_prefix": null
    },
    "borg-backups": {
      "key_name": "borg-backups",
      "capabilities": [
        "listKeys",
        "writeKeys",
        "deleteKeys",
        "listBuckets",
        "writeBuckets",
        "deleteBuckets",
        "listFiles",
        "readFiles",
        "shareFiles",
        "writeFiles",
        "deleteFiles"
      ],
      "bucket_id": null,
      "name_prefix": null
    },
    "paperless": {
      "key_name": "paperless",
      "capabilities": [
        "listKeys",
        "writeKeys",
        "deleteKeys",
        "listBuckets",
        "writeBuckets",
        "deleteBuckets",
        "listFiles",
        "readFiles",
        "shareFiles",
        "writeFiles",
        "deleteFiles"
      ],
      "bucket_id": "666-backups-kopia-paperless",
      "name_prefix": null
    }
  }
}

module "b2_app_keys" {
  source        = "git::git@gitlab.com:666_iac/terraform-modules/b2/terraform-application_key.git"
  for_each      = local.app_keys

  key_name      = each.value.key_name 
  capabilities  = each.value.capabilities
  bucket_id     = try(module.b2_buckets[each.value.bucket_id].bucket_id, null)
  name_prefix   = each.value.name_prefix
}
