locals {
  prefix = "666"

  buckets = {
    "${local.prefix}-backups-duplicacy-main": {
      "bucket_name": "${local.prefix}-backups-duplicacy-main",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-borg-backups-android": {
      "bucket_name": "${local.prefix}-borg-backups-android",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-borg-backups-certs": {
      "bucket_name": "${local.prefix}-borg-backups-certs",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-borg-backups-crashplan": {
      "bucket_name": "${local.prefix}-borg-backups-crashplan",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-borg-backups-etc": {
      "bucket_name": "${local.prefix}-borg-backups-etc",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-borg-backups-josh": {
      "bucket_name": "${local.prefix}-borg-backups-josh",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-borg-backups-linode": {
      "bucket_name": "${local.prefix}-borg-backups-linode",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-borg-backups-photos": {
      "bucket_name": "${local.prefix}-borg-backups-photos",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-borg-backups-syncthing": {
      "bucket_name": "${local.prefix}-borg-backups-syncthing",
      "bucket_type": "allPrivate"
    },
    "b2-snapshots-b8ca855351a0": {
      "bucket_name": "b2-snapshots-b8ca855351a0",
      "bucket_type": "snapshot"
    },
    "${local.prefix}-restic-backups": {
      "bucket_name": "${local.prefix}-restic-backups",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-restic-backups-josh": {
      "bucket_name": "${local.prefix}-restic-backups-josh",
      "bucket_type": "allPrivate"
    },
    "${local.prefix}-kopia-backups-paperless": {
      "bucket_name": "${local.prefix}-kopia-backups-paperless",
      "bucket_type": "allPrivate"
    }
  }
}


module "b2_buckets" {
  source        = "git::git@gitlab.com:666_iac/terraform-modules/b2/terraform-bucket.git"
  for_each      = local.buckets

  bucket_name   = each.value.bucket_name 
  bucket_type   = each.value.bucket_type 
}

