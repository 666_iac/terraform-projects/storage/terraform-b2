locals {
    secrets = {
        "b2"                      = {id = "115d8674-2952-44d7-b62e-af2b003d0920"}
    }

    app_secrets = module.secrets.secrets

    b2_account_id = local.app_secrets["b2"].username 
    b2_account_key = local.app_secrets["b2"].password
}

# grab secrets
module "secrets" {
    source  = "git::git@gitlab.com:666_iac/terraform-modules/custom/terraform-secrets.git"
    secrets = local.secrets
}