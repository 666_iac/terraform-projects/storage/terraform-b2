output "b2_buckets" {
  value = module.b2_buckets
}

output "appkeys" {
  value = {for k, v in module.b2_app_keys : k => {
    "keyId": v.application_key_id,
    "options": v.options
  }}
}
