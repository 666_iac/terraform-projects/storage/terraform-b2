provider "b2" {
    application_key = local.b2_account_key
    application_key_id = local.b2_account_id
}

provider "bitwarden" {
  master_password = var.bw_password
  client_id       = var.bw_client_id
  client_secret   = var.bw_client_secret
  email           = var.bw_email
  server          = var.bw_server
}