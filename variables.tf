variable "bw_client_id" {
    type = string
}
variable "bw_client_secret" {
    type = string 
}
variable "bw_email" {
    type = string 
}
variable "bw_password" {
    type = string 
}
variable "bw_server" {
    default = "https://vault.bitwarden.com"
}