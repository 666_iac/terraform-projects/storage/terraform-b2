terraform {
  required_version = ">= 1.0.0"
  required_providers {
    b2 = {
      source = "Backblaze/b2"
    }
    bitwarden = {
      source  = "maxlaverse/bitwarden"
      version = ">= 0.1.1"
    }
  }
}